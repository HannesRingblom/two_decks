# A random card picker

Randomizes a poker hand from two decks and displays the hand.

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Requirements

Requires `gcc`.

## Usage

Uses Cmake
```sh
# Compile the project from root folder
$ g++ src/cards.cpp -o cards
#Run the compiled binary
$ ./cards
```

## Maintainers

[Hannes Ringblom @HannesRingblom](https://gitlab.com/HannesRingblom)


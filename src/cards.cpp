#include <iostream>
#include <cstdlib> //for rand and srand
#include <cstdio>
#include <string>
#include <vector>
#include <algorithm>
#include <bits/stdc++.h>

struct Card
{
  int numValue;
  char faceValue;
  std::string suit;
};

void print_best_hand(int hand_ranking);
int check_straight_flush(std::vector<Card> hand);
int check_similar(std::vector<Card> hand);
void print_hand(std::vector<Card> hand);
bool compare( Card a, Card b);

int main() {
    std::vector<Card> deck;
    deck.push_back({2, '2', "Clubs"});
    deck.push_back({3, '3', "Clubs"});
    deck.push_back({4, '4', "Clubs"});
    deck.push_back({5, '5', "Clubs"});
    deck.push_back({6, '6', "Clubs"});
    deck.push_back({7, '7', "Clubs"});
    deck.push_back({8, '8', "Clubs"});
    deck.push_back({9,'9', "Clubs"});
    deck.push_back({10,'T', "Clubs"});
    deck.push_back({11,'J', "Clubs"});
    deck.push_back({12,'Q', "Clubs"});
    deck.push_back({13,'K', "Clubs"});
    deck.push_back({14,'A', "Clubs"});

    deck.push_back({2, '2', "Diamonds"});
    deck.push_back({3, '3', "Diamonds"});
    deck.push_back({4, '4', "Diamonds"});
    deck.push_back({5, '5', "Diamonds"});
    deck.push_back({6, '6', "Diamonds"});
    deck.push_back({7, '7', "Diamonds"});
    deck.push_back({8, '8', "Diamonds"});
    deck.push_back({9, '9', "Diamonds"});
    deck.push_back({10, 'T', "Diamonds"});
    deck.push_back({11, 'J', "Diamonds"});
    deck.push_back({12, 'Q', "Diamonds"});
    deck.push_back({13, 'K', "Diamonds"});
    deck.push_back({14, 'A', "Diamonds"});

    deck.push_back({2, '2', "Hearts"});
    deck.push_back({3, '3', "Hearts"});
    deck.push_back({4, '4', "Hearts"});
    deck.push_back({5, '5', "Hearts"});
    deck.push_back({6, '6', "Hearts"});
    deck.push_back({7, '7', "Hearts"});
    deck.push_back({8, '8', "Hearts"});
    deck.push_back({9, '9', "Hearts"});
    deck.push_back({10, 'T', "Hearts"});
    deck.push_back({11, 'J', "Hearts"});
    deck.push_back({12, 'Q', "Hearts"});
    deck.push_back({13, 'K', "Hearts"});
    deck.push_back({14, 'A', "Hearts"});

    deck.push_back({2, '2', "Spades"});
    deck.push_back({3, '3', "Spades"});
    deck.push_back({4, '4', "Spades"});
    deck.push_back({5, '5', "Spades"});
    deck.push_back({6, '6', "Spades"});
    deck.push_back({7, '7', "Spades"});
    deck.push_back({8, '8', "Spades"});
    deck.push_back({9, '9', "Spades"});
    deck.push_back({10, 'T', "Spades"});
    deck.push_back({11, 'J', "Spades"});
    deck.push_back({12, 'Q', "Spades"});
    deck.push_back({13, 'K', "Spades"});
    deck.push_back({14, 'A', "Spades"});
    std::vector<Card> deck2;
    deck2 = deck;
    std::vector<Card> hand; 

    

        std::cout<<"Press enter to draw hand: ";
        //string input;
        std::cin.ignore();
        srand(time(NULL));
        
        for (size_t i = 0; i < 5; i++)
        {
            int random_deck = rand() % 2;
            int added_card;
            if(random_deck == 1){
                added_card = rand() % deck.size();
                hand.push_back(deck[added_card]);
                deck.erase(deck.begin() + added_card);
            }else{
                added_card = rand() % deck2.size();
                hand.push_back(deck2[added_card]);
                deck2.erase(deck2.begin() + added_card);
            }
        }
        std::sort(hand.begin(), hand.end(), compare);
        print_hand(hand);
        int best_hand;
        int best_similar = check_similar(hand);
        int best_straight_flush = check_straight_flush(hand);
        if (best_similar < best_straight_flush)
        {
            best_hand = best_similar;
        }else{
            best_hand = best_straight_flush;
        }
        print_best_hand(best_hand);

        

    
}

bool compare( Card a, Card b){
	/** to use another attribute for sorting, just replace 'age' with it eg. (a.roll_no < b.roll_no)
	 the return value determines which student will go first in the sorted array **/
	if(a.numValue < b.numValue)
		return 1;
	else 
		return 0;
}

void print_hand(std::vector<Card> hand){

    for (size_t i = 0; i < 5; i++)
    {
        std::cout<<hand[i].faceValue<<" of "<<hand[i].suit<<std::endl;
    }
}

int check_straight_flush(std::vector<Card> hand){
    bool flush = false;
    int flush_count = 0;
    int best_hand = 11;
    for (size_t i = 0; i < hand.size() -1; i++)
    {
        if (hand[i].suit == hand[i+1].suit)
        {
            flush_count++;
        }
        if (flush_count == 4)
        {
            flush = true;
        }
    }
    bool straight = false;
    int straight_count = 0;
    for (size_t i = 0; i < hand.size() -1; i++)
    {
        if (hand[i].numValue == (hand[i+1].numValue - 1)){
            straight_count = straight_count + 1;
        }
        if (straight_count == 4){
            straight = true;
        }
    }
    straight_count = 0;
    if(flush){
        if(straight){
            if(hand[0].numValue == 10){
                best_hand = 2;
            }else{
                best_hand = 3;
            }
        }else{
            best_hand = 6;
        }
    }else if (straight)
    {
        best_hand = 7;
    }
    
    return best_hand;
}
int check_similar(std::vector<Card> hand){
    std::vector<int> num_values;
    for (size_t i = 0; i < hand.size(); i++)
    {
        num_values.push_back(hand[i].numValue);
    }
    

    int poker_hand;
    int most_index = 0;
    int most_occurences = 0;
    int second_most = 0;
    int current_occurences;

    for (size_t i = 0; i < hand.size(); i++)
    {
        current_occurences = std::count(num_values.begin(), num_values.end(), num_values[i]);
        if(current_occurences > most_occurences){
            most_occurences = current_occurences;
            most_index = i;
        }
        else if (current_occurences > second_most && num_values[i] != num_values[most_index])
        {
            second_most = current_occurences;
        }
    }
    if(most_occurences == 5){
        poker_hand = 1;
    }else if(most_occurences ==4){
        poker_hand = 4;
    }else if (most_occurences == 3)
    {
        if (second_most == 2)
        {
            poker_hand = 5;
        }else{
            poker_hand = 8;
        }
        
    }else if(most_occurences == 2){
        if(second_most == 2){
            poker_hand = 9;
        }else{
            poker_hand = 10;
        }
    }else{
        poker_hand == 0;
    }
    return poker_hand;
}

void print_best_hand(int hand_ranking){
    std::cout<<"The best of your hand is:  ";
    switch (hand_ranking)
    {
    case 1:
        std::cout<<"Five of a kind!!"<<std::endl;
        break;
    case 2:
        std::cout<<"Royal flush!"<<std::endl;
        break;

    case 3:
        std::cout<<"Straight flush!"<<std::endl;
        break;

    case 4:
        std::cout<<"Four of a kind!"<<std::endl;
        break;

    case 5:
        std::cout<<"Full House!"<<std::endl;
        break;

    case 6:
        std::cout<<"Flush!"<<std::endl;
        break;

    case 7:
        std::cout<<"Straight!"<<std::endl;
        break;

    case 8:
        std::cout<<"Three of a kind"<<std::endl;
        break;
    case 9:
        std::cout<<"Two pair"<<std::endl;
        break;

    case 10:
        std::cout<<"One pair"<<std::endl;
        break;

    case 11:
        std::cout<<"High card"<<std::endl;
        break;

    default:
        break;
    }
}